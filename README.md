#  Implementation of the Clustering K-Nearest Data Mining Algorithm base on Java 

The purpose of this exercise is to implement an algorithm computing the
most interesting k subgroups for datasets over binary attributes and binary
target classes.

#  Technical requirements:

- The INPUT should be an ASCII text file consisting of m + 1 lines:
{ Line 1 contains the values for the parameters m, n, k, and p.
{ Line i + 1 is the binary sequence of length n + 1 corresponding
to the attribute values of instance i (i = 1; : : : ; m).
- The OUTPUT (ASCII text file) should consist of (at most) k lines,
where line i contains the monomial corresponding to the i-th best
hypothesis and the value of qp for this hypothesis.
- Make sure to comment the code!


# Note: 
Import as eclipse project.