package knear;

import java.util.List;
import java.util.Random;
import java.util.Vector;

public class main {

		double [] sumW,thresholdW ;
		double [][] w; //where w[1][0] correspond neuron weight w1 with input x0
        Integer [] x; //inputs
        Integer [] y,t;//outputs
        Integer numberX=0,numberY=0;
        int tp=0,tn=0,fp=0,fn=0;
        FileManager file;
        Util number = new Util(); 
 
        
        public void processinputData(){
        	

            
            
            Random generator = new Random();
            
            for(int k=1;k<4;k++) {
            	int accuracy =0;
            	System.out.println("\nNew k : "+k);
               	tp=0;
            	tn=0;
            	fp=0;
            	fn=0;
            	
	            for(int i=0;i<5;i++){

	            	file = new FileManager ();
	                file.readSampleFile(true);
	                Knearest kn = new Knearest();
	                
	            	double randomNumber =generator.nextDouble();
	          	  	double value=randomNumber*file.sampleTesting.size();
		          	int pos =  (int)value;  
		          	
		          	String [] instanceLine= file.sampleTesting.get(0);
		          	String str ="[";
		          	for (int j=0;j<instanceLine.length;j++){
		          		str +=instanceLine[j]+" ";
		          		
		          	}
		          	System.out.println("\n>Instance test: "+str+"]");
		          	System.out.println(" numberTest:"+i+" ,pos to test:"+pos+" valuedouble:"+value+" randomNumber:"+randomNumber+" sampleTesting.size:"+file.sampleTesting.size());
		            kn. createDistanceList(file.sampleTesting.remove(0),file.sampleLearning, file.listAttribute);
		            String h=kn.predictTargetInstance(k);
		            System.out.println(" h prediction ="+ h);
		            
		            if(h.equals(instanceLine[instanceLine.length-1])){
		            	System.out.println(">prediction OK!\n");
		            	accuracy++;
		            	if (h.equals("yes")) tp++;
		            	else tn++;
		            }
		            else{
		            	System.out.println(">prediction Fail!\n");
	            		if (h.equals("yes")) fp++;
	            		else fn++;
		            }
		 
	            }
	            double prec = 0.0;
	            double rec = 0.0,F1=0.0;
	            
	            if ((tp+fp) >0.0)
	            	prec = tp/(tp+fp)*1.0;
	            
	            if ((tp+fn) >0.0)
	            	rec = tp/(tp+fn)*1.0;
	            
	            if(prec>0.0&rec>0.0){
	            	F1=1/(0.5/prec + 0.5/rec);
	            }
	            else
	                if(prec==0.0&rec==0.0){
		            	F1=0.0;
		            }
	                else
			            if(prec==0.0){
			            	F1=1/(0.5/rec);
			            }
			            else
				            if(rec==0.0){
				            	F1=1/(0.5/prec);
				            }
		            	
	            double acur = (tp+tn)*1.0/5.0*100;
	            
	            System.out.println("\n>> k ="+k+", Accuracy :"+acur+"% ,Pre :"+prec+" ,Rec:"+rec+" , tp:"+tp+", tn:"+tn+" , fp:"+fp+", fn:"+fn+", F(0.5)="+F1+"\n");
            }
            
            
            //System.out.println("createDistaceMatrix:");
 
            

        }


    
	public void simulate() {
   
        processinputData();

                 
	}
	
	public static void main(String[] args) {
		
		main objMain = new main();
	printMainMenu(objMain);
               if (args.length>0) {
              //  objMain.numP=Integer.parseInt(args[0]);
              //  objMain.numU=Integer.parseInt(args[1]);
               // objMain.numL=Integer.parseInt(args[2]);
               }
		objMain.simulate();
                 System.exit(0);
	}
	
	/**
	 * Prints the main menu in the console
	 */
	private static void printMainMenu(main objMain) {
		
    	System.out.println("----------------------------------------------------------------------");
    	System.out.println("- Universitat Bonn, Intelligent Learning and Information Systems WS2014. K-Nearest");
    	System.out.println("----------------------------------------------------------------------");
		System.out.println("** -- Klaus Martinez");
		System.out.println("** -- Camilo Morales");
		System.out.println("*************************************************************");
    	System.out.println("\n");
    	System.out.println("Running Algorithm...");
	}
}
