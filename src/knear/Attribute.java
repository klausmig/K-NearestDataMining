package knear;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Attribute {
	String name="",type="";
	int ratio=0;
	List<ValueAttribute> values = new  ArrayList<ValueAttribute>();
	List<ValueAttribute> bestSubsetSCategorical;
	
	boolean classified=false;
	int pos=0;
	double maxEntropy=0.0,bestSplitNumerical=0.0;
	boolean changesEntropy=true;
	
	double log2(double x) {  
	     return Math.log(x)/Math.log(2.0d);  
	}  
	
	
	
	public String toString1(){
		
		String str="(name="+name+",type="+type+",values={";
		for(int j=0;j<values.size();j++){
			str+=values.get(j).value+",";
		}
		str+="} classified:"+classified+")\n";
		return str;
		
	
		
	}
	
	public void scaleAttribute(){
		if (type.equals("n")) {
			order(values);
			ratio=Integer.parseInt(values.get(values.size()-1).value)-Integer.parseInt(values.get(0).value);
		}
		else ratio=1;

	}
	
	public String toString(){
		String str="";
		
		if(!this.classified){
		str="(name="+name+",type="+type+",values={";
		for(int j=0;j<values.size();j++){
			str+=values.get(j).value+",";
		}
		str+="} ,ratio:"+ratio+")\n";
		
		}
		return str;
	
	}
	
	public Attribute clone() {
        Attribute temp = new Attribute ();
        temp.name =this.name;
        temp.values =this.values;
        temp.type =this.type;
        temp.pos =this.pos;
        temp.bestSubsetSCategorical =this.bestSubsetSCategorical;
        temp.classified =this.classified;
        temp.maxEntropy =this.maxEntropy;
        temp.bestSplitNumerical =this.bestSplitNumerical;

        return temp;
        
    }
	
	
    public void order(List<ValueAttribute> listValueAttribute) {

        Collections.sort(listValueAttribute, new Comparator() {
            public int compare(Object o1, Object o2) {
            	
            	ValueAttribute x1array = ((ValueAttribute) o1);
            	ValueAttribute x2array = ((ValueAttribute) o2);
                

                
                int x1 = Integer.parseInt(x1array.value);
                int x2 = Integer.parseInt(x2array.value);
                
               // System.out.println("x1:"+x1+" x1array.distance:"+x1array.distance+" ,x2:"+x2);
                
                if (x1 !=x2) {
                    return x1 - x2;
                } else {
                	return x2 - x1;
                }
            }
        });
    }
	

}
