package knear;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Knearest {
	
	double [][]distanceMatrix;
	List <Neighbor> listNeighbor ;

	public String predictTarget(int k ,int posInstance,List<String[]> sample){

		listNeighbor =new  ArrayList<Neighbor>();
		
		
		for (int i=0;i<distanceMatrix.length;i++){
			if(i!=posInstance){
				Neighbor instance = new Neighbor();
				instance.distance=distanceMatrix[posInstance][i];
				instance.pos=i;
				instance.target=sample.get(i)[sample.get(i).length-1];
				listNeighbor.add(instance);
			}
		}
		order(listNeighbor);
		System.out.println("posInstance:"+posInstance +" listNeighbor:"+listNeighbor.toString()+"\n");
		
		int targetYes=0,targetNo=0;
		for (int i=0;i<k;i++){
			
				Neighbor instance = listNeighbor.get(i);
				if (instance.target.equals("yes"))
					targetYes++;
				else
					targetNo++;
	
			
		}
		if (targetYes>targetNo)
			return "Yes (Yes:"+targetYes+">No:"+targetNo+")";
		else return "No";
		
	}
	
	
	public String predictTargetInstance(int k){
		
		
      	
      	String str ="[";
      	for (int j=0;j<6;j++){
      		Neighbor instance= listNeighbor.get(j);
      		 str+="[pos("+instance.pos+"),distance("+instance.distance+"),target("+instance.target+")],";
      	}
      	str+="...]";


		System.out.println(" listNeighbor:"+str);
		
		//System.out.println(" listNeighbor:"+listNeighbor.toString()+"\n");
		
		int targetYes=0,targetNo=0;
		for (int i=0;i<k;i++){
			
				Neighbor instance = listNeighbor.get(i);
				if (instance.target.equals("yes"))
					targetYes++;
				else
					targetNo++;
	
			
		}
		/*if (targetYes>targetNo)
			return "Yes (Yes:"+targetYes+">No:"+targetNo+")";
		else return "No (Yes:"+targetYes+"<No:"+targetNo+")"; */
		
		if (targetYes>targetNo)
			return "yes";
		else 
			return "no";
		
	}
	
	/* distance for numerical attributes is euclidian distance, 
	 * for categorical or boolean  d(attrib1,attrib2)=0 if attrib1=attrib2 else d(attrib1,attrib2) =1
	 */
	
	public double euclidianDistance(String [] xi,String[] xj,List<Attribute> listAttribute ){
		
		double distance =0.0;
		for (int i=0;i<listAttribute.size();i++){
			
			char c = listAttribute.get(i).type.charAt(0);
			switch(c){
				case 'n': 
					distance += Math.pow((Double.parseDouble(xi[i])-Double.parseDouble(xj[i]))/listAttribute.get(i).ratio, 2);
					//System.out.println("type '"+listAttribute.get(i).type+" difference:"+Math.pow(Double.parseDouble(xi[i])-Double.parseDouble(xj[i]), 2));
					break;
				case 'c': 
				case 'b':
					if (!(xi[i].equals(xj[i]))){
						distance+=1.0;
						//System.out.println("type '"+listAttribute.get(i).type+" difference:1 xi[i]:"+xi[i]+" xj[i] "+xj[i]);
						
					}
					else
						//System.out.println("type '"+listAttribute.get(i).type+" difference:0 xi[i]:"+xi[i]+" xj[i] "+xj[i]);
					
					//System.out.println("Choosed Best Attribute '"+listAttribute.get(posAttributeSplit).name+"' Split S:"+listAttribute.get(posAttributeSplit).bestSubsetSCategorical.toString()+" Entropy:"+listAttribute.get(posAttributeSplit).maxEntropy+"\n");
					break;
		
			}
	
	
		}
		distance = Math.sqrt(distance);
		//System.out.println("distance:"+distance);
		return distance;
	}
	
	public void printDistanceMatrix(){
		
		for (int i=0;i<distanceMatrix.length;i++){
			String s="\n[";
			
			//System.out.println();
			for (int j=0;j<distanceMatrix.length;j++){
				s+=distanceMatrix[i][j]+" ";	
			}
			System.out.println(s+"]");
		}
		
	}
	
	public void createDistanceList(String[] instanceTest,List<String[]> learningSample,List<Attribute> listAttribute ){

		listNeighbor =new  ArrayList<Neighbor>();
		
		for (int i=0;i<learningSample.size();i++){
			
				Neighbor instance = new Neighbor();
				
				double valueDistance=euclidianDistance(instanceTest,learningSample.get(i),listAttribute );
				instance.distance=valueDistance;
				instance.pos=i;
				instance.target=learningSample.get(i)[learningSample.get(i).length-1];
				listNeighbor.add(instance);
		
		}
		
		order(listNeighbor);

	}
	
	
	public void createDistaceMatrix(List<String[]> sample,List<Attribute> listAttribute ){

		distanceMatrix = new double [sample.size()][sample.size()];
		
		
		for (int i=0;i<sample.size();i++){
			for (int j=i;j<sample.size();j++){
				if (j!=i){
					double valueDistance=euclidianDistance(sample.get(i),sample.get(j),listAttribute );
					distanceMatrix[i][j]=valueDistance;
					distanceMatrix[j][i]=valueDistance;
				}
				else 
					distanceMatrix[j][i]=0.0;
				
			}
		}
	}
		
	private int round(double d){
	    double dAbs = Math.abs(d);
	    int i = (int) dAbs;
	    double result = dAbs - (double) i;
	    if(result<0.5){
	        return d<0 ? -i : i;            
	    }else{
	        return d<0 ? -(i+1) : i+1;          
	    }
	}
	
    public void order(List<Neighbor> listNeighbor) {

        Collections.sort(listNeighbor, new Comparator() {
            public int compare(Object o1, Object o2) {
            	
            	Neighbor x1array = ((Neighbor) o1);
            	Neighbor x2array = ((Neighbor) o2);
                

                
                int x1 = (int)(x1array.distance*1000);
                int x2 = (int)(x2array.distance*1000);
                
               // System.out.println("x1:"+x1+" x1array.distance:"+x1array.distance+" ,x2:"+x2);
                
                if (x1 !=x2) {
                    return x1 - x2;
                } else {
                	return x2 - x1;
                }
            }
        });
    }


	

}
