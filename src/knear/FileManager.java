/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package knear;

import java.io.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Pattern;
import java.util.Random;

import javax.swing.*;


public class FileManager {
    
    String axiom="",labelTime="";
    String[] pointLines;
    int totalPositive=0;
    
    private String pathArchivo="../data/",nombreArchivo="data_exercise_1.csv";    
    
    Hashtable<String, String> ruleTable = new Hashtable<String, String>();
    List<String[]> sampleLearning;
    List<String[]> sampleTesting;
    List<Attribute> listAttribute ;
    RandomAccessFile file ;

    @SuppressWarnings("deprecation")
    
    
    public void printSample(List<String[]> sample){
    	 for(int i=0;i<sample.size();i++){
    		 String [] line = sample.get(i);
    		 String str="[";
    		 for(int j=0;j<line.length;j++){
    			 str +=line[j]+" ";
    		 }
    		 System.out.println(str+"]");
    	 }
    	
    	
    	
    }

    
	public boolean readSampleFile(boolean esNuevo) {

	    String str;
	    boolean isValid = false;
/*
	   
		    FileDialog fd = new FileDialog(new JDialog(), "Choose a input file: Positions_PA-F.txt");
		    
	
		    if (esNuevo) {
		    	// Show dialog requesting file to be specified
		        fd.show();
		        
		        if (fd.getFile()!=null) {	
		            pathArchivo=fd.getDirectory();
		            nombreArchivo=fd.getFile();
		        }
		    }
*/
		    try {
         //BufferedReader in = new BufferedReader(new FileReader(pathArchivo+nombreArchivo));
          BufferedReader in = new BufferedReader(new FileReader("data/"+nombreArchivo));
          
          //ArrayList<String> lstConfigLines = new ArrayList<String>();

		 sampleLearning= new ArrayList<String[]>();
		 sampleTesting= new ArrayList<String[]>();
		 listAttribute =new  ArrayList<Attribute>();
          
         if((str = in.readLine()) != null){
        	 String [] dataLine = str.split(",");
        	 for(int i=0;i<dataLine.length-1;i++){
	        	 Attribute newAttr= new Attribute();
	        	 String [] vals=dataLine[i].split(":");
	        	 newAttr.name=vals[0];
	        	 newAttr.type=vals[1];
	        	 newAttr.pos=i;
	        	 listAttribute.add(newAttr);
        	 }
        	 
         }
          // Extract configuration lines from file and store them in array list to feed the validation method
          while ((str = in.readLine()) != null) {
        	 // lstConfigLines.add(str);
        	  //System.out.println(str);
        	  String [] dataLine = str.split(",");
        	  if (dataLine[dataLine.length-1].equals("yes")) totalPositive++;
        	  for(int i=0;i<dataLine.length-1;i++){
        	  	//if(i==1){
        	  	Attribute attr=listAttribute.get(i);
        	  	boolean foundAttributeValue=false;

        	  	for(int j=0;j<attr.values.size();j++){
        	  		ValueAttribute valueAttr= attr.values.get(j);
        	  		//System.out.println("match:valueAttr.value:"+valueAttr.value+" dataLine[i]:"+dataLine[i]);
        	  		if(valueAttr.value.equals(dataLine[i])){
        	  			
        	  			valueAttr.total++;
        	  			if (dataLine[dataLine.length-1].equals("yes"))
    	  					valueAttr.positive++;
        	  			foundAttributeValue=true;
	  					break;
    	  					
        	  		}
        	  				
        	  	}
        	  	
        	  	if (!foundAttributeValue){
        	  		ValueAttribute valueAttr= new ValueAttribute();
        	  		valueAttr.value=dataLine[i];
    	  			if (dataLine[dataLine.length-1].equals("yes"))
	  					valueAttr.positive++;
        	  		valueAttr.total=1;
        	  		attr.values.add(valueAttr);		
        	  		
        	  	}			
        	  }
             
        	  	 
        	  sampleLearning.add(dataLine);
          }
         // int numberTesting =(int)(sampleLearning.size()*0.30);
          int numberTesting =1;
          Random generator = new Random();
          for(int i=0;i<numberTesting;i++){
        	  double randomNumber =generator.nextDouble();
        	  double value=randomNumber*sampleLearning.size();
        	int pos =  (int)value;  
        	//System.out.println("numberTesting:"+i+" ,pos add test:"+pos+" valuedouble:"+value+" randomNumber:"+randomNumber+" sampleLearning.size:"+sampleLearning.size());
        	sampleTesting.add(sampleLearning.remove(pos));
          }
          
          System.out.println(" sample learning:  "+sampleLearning.size()+" samples");
          //printSample(sampleLearning);
          System.out.println(" sample testing:  "+sampleTesting.size()+" samples");
          
          //printSample(sampleTesting);
         
          for (int i=0;i<listAttribute.size();i++){
        	  
        	  listAttribute.get(i).scaleAttribute();
          }
          
          //System.out.println("\n Attributes:\n");
          //System.out.println(listAttribute.toString());
          
          
          //pointLines = new String[lstConfigLines.size()];
          //lstConfigLines.toArray(pointLines);
          
          // Validate configuration lines
         // isValid = validateSystemConfig(arrConfigLines);
          
          //System.out.println(lstConfigLines.get(0));
          
          in.close();

        } catch (IOException e) {
        	System.out.println("Error reading file. " + e.toString());
        }
	    
	    return isValid;

    }
    
 
 	/**
	 * Validates the D0L-System configuration represented by the set of lines
	 * provided as parameter.
	 * @param configLines Array containing each of the configuration lines of the system as one of its elements.
	 */


   public boolean openFile(String labelName,String header,boolean newTime)
    {
        if (pathArchivo=="")
        {
          FileDialog fd= new FileDialog(new JDialog() ,"Choose a file:",FileDialog.SAVE);
          fd.show();
           if (fd.getFile()!=null) 

           pathArchivo=fd.getDirectory();
           nombreArchivo=fd.getFile();
         // lArchivo.setText("Archivo de Trabajo: "+pathArchivo+nombreArchivo);
        } 
        if (newTime) labelTime= ""+System.currentTimeMillis( );
        try {
              nombreArchivo=labelName+labelTime+".txt";
              file = new  RandomAccessFile(pathArchivo+nombreArchivo,"rw");


             String line="# This file is called "+nombreArchivo+"\n"+ header; 

             file.writeBytes(line);
             
             
               // archivo.close();
            } catch (IOException e) {
                
                return false;
            }

      return true;  
    }
   
   
   public void saveFile(String line)
    {

        try {
             file.writeBytes(line);
           
            } catch (IOException e) {
            }


    }   
   
   
      public void closeFile()
    {

        try {
             
             file.close();
            } catch (IOException e) {
            }


    }   


    /*private void saveSimFile(String solucion)
    {
        if (pathArchivo=="")
        {
          FileDialog fd= new FileDialog(new JDialog() ,"Elija un Archivo para Guardar:",FileDialog.SAVE);
          fd.show();
           if (fd.getFile()!=null) 

           pathArchivo=fd.getDirectory();
           nombreArchivo=fd.getFile();
         // lArchivo.setText("Archivo de Trabajo: "+pathArchivo+nombreArchivo);
        } 

        try {

             RandomAccessFile archivo = new  RandomAccessFile(pathArchivo+nombreArchivo,"rw");




             archivo.writeBytes(solucion);
                archivo.close();
            } catch (IOException e) {
            }


    }

    private void saveSimulation(String simulacion){

    try {
          //  
         RandomAccessFile archivo2 = new  RandomAccessFile(pathArchivo+"simulacion.txt","rw");

       //  archivo.seek(archivo.length());






          archivo2.writeBytes(simulacion);
            archivo2.close();
        } catch (IOException e) {
        }



    }    */
    
}
