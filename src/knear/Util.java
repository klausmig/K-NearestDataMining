package knear;

/**
 * Implements general, utility methods commonly used
 * @author Klaus Martinez, Santiago Londoño
 */
public class Util {
	
	/**
	 * Rounds a number to a specified amount of decimal positions	
	 * @param number Number to be rounded
	 * @param digits Amount of decimal positions to round the number to
	 * @return Number rounded to the specified amont of decimal positions
	 */
	public  double round(double number, int digits) {

		int cifras = (int) Math.pow(10, digits);
		return Math.rint(number * cifras) / cifras;
	}

	/**
	 * Generates an uniformly distributed random number in the range specified by the parameters
	 * @param min Minumum value the random number will take 
	 * @param max Maximum value the random number will take
	 * @return uniformly distributed random number in the range [min, max]
	 */
	public  double generatRandomPositiveNegitiveValue(double min, double max) {

		double ii = (min + Math.random() * (max - min));
		return ii;
	}
	
	public String printArray(double [] array){
		
		String value="";
    	for (int i=0; i<array.length;i++){
    		value += round(array[i],2)+" ";
    	}
		
		return value;
		
	}

}
